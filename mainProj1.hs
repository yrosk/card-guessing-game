module Main (main) where

import Proj1

main :: IO ()
main = do
    let poss1 = allPossibleHands 1
    let bestGuess1 = getBestGuess poss1
    putStrLn (show bestGuess1)

    let poss2 = allPossibleHands 2
    let bestGuess2 = getBestGuess poss2
    putStrLn (show bestGuess2)

    let poss3 = allPossibleHands 3
    let bestGuess3 = getBestGuess poss3
    putStrLn (show bestGuess3)

    let poss4 = allPossibleHands 4
    let bestGuess4 = getBestGuess poss4
    putStrLn (show bestGuess4)
