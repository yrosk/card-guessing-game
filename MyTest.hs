--  File     : publictests.hs
--  Author   : Peter Schachte
--  Modified : Skye Rajmon McLeman
--  Purpose  : test cases for Assignment1 project

import Card
import Proj1
import HaskellTest

suite = 
  TimeLimit 2.0 $
  Suite [
  {-- NOT NEEDED ANYMORE
    expect (lowestRank []) (error "Empty list has no lowest rank"),
    expect (lowestRank [(Card Spade Ace),(Card Club R9)]) (R9),
    expect (lowestRank [(Card Diamond King),(Card Diamond King)]) (King),
    expect (lowestRank [(Card Heart R7),(Card Heart R2)]) (R2),

    expect (highestRank []) (error "Empty list has no highest rank"),
    expect (highestRank [(Card Spade Ace),(Card Club R9)]) (Ace),
    expect (highestRank [(Card Diamond King),(Card Diamond King)]) (King),
    expect (highestRank [(Card Heart R7),(Card Heart R2)]) (R7),
    --}

{--=
    expect (numMatches (Card Spade Ace) []) (0),
    expect (numMatches (Card Spade Ace) [(Card Spade Ace),(Card Heart R4)]) (1),
    expect (numMatches (Card Spade Ace) [(Card Spade Ace),(Card Heart R4),(Card Spade Ace)]) (2),
--}

    expect (cardsCorrect [] [(Card Spade Ace)]) (0),
    expect (cardsCorrect [(Card Spade Ace),(Card Diamond R2)] [(Card Heart R9),(Card Heart R6)]) (0),
    expect (cardsCorrect [(Card Spade Ace),(Card Diamond R2)] [(Card Spade Ace),(Card Club R8)]) (1),
    expect (cardsCorrect [(Card Spade Ace),(Card Club R8)] [(Card Spade Ace),(Card Club R8)]) (2),

    expect (rankLower [(Card Diamond R4),(Card Diamond R2)] [(Card Diamond R3),(Card Diamond Ace)]) (1),
    expect (rankLower [(Card Diamond R4),(Card Diamond R2)] [(Card Diamond R2),(Card Diamond Ace)]) (0),
    expect (rankLower [(Card Diamond R4),(Card Diamond R2)] [(Card Diamond R5),(Card Diamond Ace)]) (2),

    expect (rankHigher [(Card Diamond R4),(Card Diamond R2)] [(Card Diamond R3),(Card Diamond Ace)]) (0),
    expect (rankHigher [(Card Diamond R4),(Card Diamond R2)] [(Card Diamond R2),(Card Diamond R3)]) (1),
    expect (rankHigher [(Card Diamond R4),(Card Diamond R3)] [(Card Diamond R2),(Card Diamond R2)]) (2),

    --expect (containsSuit [(Card Spade Ace),(Card Heart R2),(Card Diamond R2)]) ((False,True,True,True)),
    --expect (containsSuit []) ((False,False,False,False)),
    --expect (containsSuit [(Card Diamond R7)]) ((False,True,False,False)),

    {--
    expect (numEachSuit [(Card Diamond R7),(Card Heart R3),(Card Heart King)]) ((0,1,2,0)),
    expect (numEachSuit []) ((0,0,0,0)),
    expect (numEachSuit [(Card Club King),(Card Spade Ace),(Card Diamond R2),(Card Heart Jack)]) ((1,1,1,1)),
    --}

    expect (suitSame [(Card Spade Ace),(Card Heart R2)] [(Card Spade R3),(Card Heart R2)]) (2),
    expect (suitSame [(Card Spade Ace),(Card Heart R2),(Card Heart R3)] [(Card Spade R3),(Card Heart R2),(Card Club R9)]) (2),
    expect (suitSame [(Card Spade Ace),(Card Heart R2),(Card Heart R3)] [(Card Spade R3),(Card Spade R7),(Card Heart R2)]) (2),
    expect (suitSame [(Card Heart R2)] [(Card Spade R3),(Card Heart R2)]) (1),
    expect (suitSame [] [(Card Spade R3),(Card Heart R2)]) (0),
    expect (suitSame [(Card Heart R2)] []) (0),
    expect (suitSame [] []) (0),
    expect (suitSame [(Card Spade Ace),(Card Heart R2),(Card Heart R3),(Card Club R9)] [(Card Club King),(Card Spade R3),(Card Heart R2)]) (3),
    --v test failed
    expect (suitSame [(Card Diamond Queen),(Card Spade Ace),(Card Heart R2),(Card Heart R3),(Card Club R9)] [(Card Club King),(Card Spade R3),(Card Heart R2),(Card Diamond Jack),(Card Diamond R3)]) (4),
    expect (suitSame [(Card Club R2),(Card Club R6)] [(Card Club R2),(Card Club R7)]) (2),

{--
    expect (listContains (Card Spade Ace) [(Card Diamond Queen),(Card Spade Ace)]) (True),
    expect (listContains (Card Spade R3) [(Card Diamond Queen),(Card Spade Ace)]) (False),
    expect (listContains (Card Spade R3) []) (False),

    expect (getDistinctRanks [(Card Heart R7),(Card Heart R8),(Card Spade R7)]) ([R8,R7]),
    expect (getDistinctRanks [(Card Heart R7)]) ([R7]),
    expect (getDistinctRanks []) ([]),

    expect (numInBoth [R2,R3,Jack] [R2]) (1),
--}

    expect (rankSame [(Card Diamond Queen),(Card Diamond Ace)] [(Card Diamond R2),(Card Diamond R3)]) (0),
    expect (rankSame [(Card Diamond Queen),(Card Diamond Ace)] [(Card Diamond R2),(Card Diamond Ace)]) (1),
    expect (rankSame [(Card Diamond Queen),(Card Diamond Ace)] [(Card Diamond Queen),(Card Diamond Ace)]) (2),
    expect (rankSame [] [(Card Spade King)]) (0),
    expect (rankSame [(Card Spade King)] [] ) (0),

    expect (rankSame [(Card Diamond R3),(Card Heart R3)] [(Card Spade R3),(Card Club R3)]) (2),
    --}

    expect (feedback [(Card Club R3),(Card Heart R4)] [(Card Heart R4),(Card Club R3)]) ((2,0,2,0,2)),
    expect (feedback [(Card Club R3),(Card Heart R4)] [(Card Club R3),(Card Heart R3)]) ((1,0,1,1,2)),
    expect (feedback [(Card Diamond R3),(Card Heart R3)] [(Card Spade R3),(Card Club R3)]) ((0,0,2,0,0)),
    expect (feedback [(Card Club R3),(Card Heart R4)] [(Card Heart R2),(Card Heart R3)]) ((0,0,1,1,1)),
    expect (feedback [(Card Club Ace),(Card Club R2)] [(Card Club R3),(Card Heart R4)]) ((0,1,0,1,1))

    ]

main :: IO ()
main = do
  testVerbose suite
