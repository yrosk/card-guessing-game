{-
    COMP90048 Declarative Programming
    Semester 2, 2016

    Project 1

    Skye Rajmon McLeman

    8 September, 2016
-}

-- This program implements a two-player logical guessing game. Each of the
-- hints suggested in the specification are used to achieve sufficient
-- quality of the guesses while also satisfying the testing system's time
-- constraints.

module Proj1 (feedback, initialGuess, nextGuess, GameState,
    cardsCorrect, rankLower, rankHigher, suitSame, rankSame,
    allPossibleHands, getBestGuess) where 

import Card
import Data.List
import Debug.Trace

-- These types are effectively just aliases to improve readability.
type Hand = [Card]
type RemHands = [Hand]
type GuessFeedback = (Int,Int,Int,Int,Int)
type PrevGuesses = [(Hand, GuessFeedback)]

-- The GameState stores a list of the remaining possible guesses in a
-- game, and a list of all previous guesses.
data GameState = GameState RemHands PrevGuesses
    deriving Show

-- Takes the number of cards in the answer as input and returns a pair 
-- containing an initial guess and a game state.
-- Note that the guesses for n <= 4 have been hardcoded for speed.
initialGuess :: Int -> (Hand, GameState)
initialGuess 1 =
    let
        allPoss = allPossibleHands 1
        initial = (read "[8C]")::[Card]
        remaining = allPoss \\ [initial]
    in
        (initial, (GameState (remaining) []))
initialGuess 2 =
    let
        allPoss = allPossibleHands 2
        initial = (read "[6S,JS]")::[Card]
        remaining = allPoss \\ [initial]
    in
        (initial, (GameState (remaining) []))
initialGuess 3 =
    let
        allPoss = allPossibleHands 3
        initial = (read "[5S,9S,QS]")::[Card]
        remaining = allPoss \\ [initial]
    in
        (initial, (GameState (remaining) []))
initialGuess 4 =
    let
        allPoss = allPossibleHands 4
        initial = (read "[4S,7S,TS,KS]")::[Card]
        remaining = allPoss \\ [initial]
    in
        (initial, (GameState (remaining) []))
initialGuess n =
    let
        (h:hs) = allPossibleHands n
    in
        (h, (GameState hs []))  -- start with no prev guesses

-- Takes the previous guess and game state and feedback as input, 
-- then returns a new guess and game state.
nextGuess :: (Hand, GameState) -> GuessFeedback -> (Hand, GameState)
nextGuess (guess, (GameState [] prevGuesses)) feedback = error "Ran out of candidate hands"
nextGuess (guess, (GameState (remHands) prevGuesses)) feedback = 
    let 
        -- List of answers that pass consistency test
        consistent = filter (flip isConsistentAll ((guess,feedback):prevGuesses)) remHands

        bestGuess = 
            if
                (length consistent) <= 1000
                -- This threshold allows us to pass all the tests within the required timeframe
                -- whilst also obtaining maximum points for quality.
            then
                getBestGuess consistent
            else
                (head consistent)

        remainingGuesses = consistent \\ [bestGuess]
    in
        (bestGuess, (GameState (remainingGuesses) ((guess, feedback):prevGuesses)))

-- Returns true if a guess is consistent with all previous guesses.
isConsistentAll guess prevGuesses = all (isConsistent guess) prevGuesses

-- Returns true if a guess is consistent with a single previous guess.
isConsistent :: Hand -> (Hand, GuessFeedback) -> Bool
isConsistent guess (prevGuess, prevGuessFB) =
    feedback guess prevGuess == prevGuessFB

-- Takes a list of candidate hands as input and returns the best guess using 
-- the algorithm described in Hint 3 of the project specification. I.e. it
-- returns the guess with the smallest expected number of remaining answers.
getBestGuess :: [Hand] -> Hand
getBestGuess hands = minGuessAverage (getGuessAverages hands)

-- Returns the guess which would produce the smallest number of answers on
-- average.
minGuessAverage :: [(Hand, Float)] -> Hand
minGuessAverage [] = error "No min average for empty list"
minGuessAverage (x:xs) = minGuessAverage' x xs

minGuessAverage' (hand, val) [] = hand
minGuessAverage' (minHand, minVal) ((hand,val):xs)
    | val < minVal  = minGuessAverage' (hand,val) xs
    | otherwise     = minGuessAverage' (minHand,minVal) xs

-- Returns a list containing the number of answers on average that would be 
-- left after making each of the given guesses.
getGuessAverages :: [Hand] -> [(Hand, Float)]
getGuessAverages [] = []
getGuessAverages list = getGuessAverages' list list

getGuessAverages' [] list = []
getGuessAverages' (h:hs) list = 
    let
        feedbackList = map (feedback h) list  -- Gets list of feedback tuples
        counts = map length (group feedbackList)
        numAns = length feedbackList
        vals = map (\x -> (fromIntegral x) / (fromIntegral numAns) * (fromIntegral x)) counts
        average = foldl (+) 0 vals
    in
        -- Return list of pairs of each guess and its respective average.
        (h, average):(getGuessAverages' hs list)

-- Returns list of all possible hands of the specified size.
allPossibleHands :: Int -> [Hand]
allPossibleHands n =
    let 
        deck = [minBound..maxBound]::Hand
        -- We generate the deck and pass it to the auxiliary function
    in
        allPossibleHands' n deck
-- Auxiliary function for all possible hands function.
allPossibleHands' 0 _ = []
allPossibleHands' 1 deck = map (\x -> [x]) deck
allPossibleHands' 2 deck = 
        [c1:c2:[] | c1 <- deck, c2 <- deck, c1<c2]
allPossibleHands' 3 deck = 
        [c1:c2:c3:[] | c1 <- deck, c2 <- deck, c3 <- deck, c1<c2 && c2<c3]
allPossibleHands' 4 deck = 
        [c1:c2:c3:c4:[] | c1 <- deck, c2 <- deck, c3 <- deck, c4 <- deck, c1<c2 && c2<c3 && c3<c4]
        -- Using the list comprehension manually like this seems to be about 3x faster
        -- than the more generic function used for n > 4; so this is hardcoded for n <= 4.
allPossibleHands' n deck =
    let
        allcombos = [h1++h2 | h1 <- (allPossibleHands 1), h2 <- (allPossibleHands (n-1))] 
    in
        filter (\x -> (isAscending x) && (hasNoRepeats x)) allcombos

hasNoRepeats l = l == (nub l)

isAscending :: (Ord a) => [a] -> Bool
isAscending [] = True  -- Vacuously true that empty list is ascending
isAscending [x] = True
isAscending (x1:x2:xs) = x1 <= x2 && isAscending (x2:xs)

-- Takes a target hand, a guess, and returns feedback as a quintuple.
feedback :: Hand -> Hand -> GuessFeedback
feedback target guess =
    (cardsCorrect target guess,
     rankLower target guess,
     rankSame target guess,
     rankHigher target guess,
     suitSame target guess)

-- Returns the number of cards that match in two different hands.
cardsCorrect :: Hand -> Hand -> Int
cardsCorrect target guess = length (intersect target guess)

-- Abstracts the rankLower/rankHigher functions
rankCompare :: Hand -> Hand -> (Rank -> Bool) -> Int
rankCompare answer guess f = length (filter f (map rank answer))

-- Returns the number of cards in answer that have rank lower than the lowest rank in the guess.
rankLower :: Hand -> Hand -> Int
rankLower answer guess =
    let lowest = minimum (map rank guess) in
    rankCompare answer guess (lowest >)

-- Returns the number of cards in answer that have rank higher than the highest rank in the guess.
rankHigher :: Hand -> Hand -> Int
rankHigher answer guess = 
    let highest = maximum (map rank guess) in
    rankCompare answer guess (highest <)

-- Abstracts the rankSame/suitSame functions
rankSuitSame answer guess f = 
    let
        ans = map f answer
        gss = map f guess
    in
       length (ans \\ (ans \\ gss))

-- Returns the number of cards in answer that have the same rank as a card in the guess.
rankSame :: Hand -> Hand -> Int
rankSame answer guess = rankSuitSame answer guess rank

-- Returns the number of cards in answer that have the same suit as a card in the guess.
suitSame :: Hand -> Hand -> Int
suitSame answer guess = rankSuitSame answer guess suit
